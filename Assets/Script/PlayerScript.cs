﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour 
{
    public float JumpForce;
    public float Speed;

    private GameObject _Camera;
    private bool _InAir;
    private Rigidbody _Rigidbody;
    
    // Use this for initialization
    void Start() 
    {
        _Rigidbody = this.GetComponent<Rigidbody>();   
        _Rigidbody.constraints |= RigidbodyConstraints.FreezeRotation;
        
        _Camera = GameObject.FindWithTag("MainCamera");
        _InAir = false;
    }
    
    // Update is called once per frame
    void FixedUpdate() 
    {
        Vector3 moveVector = Vector3.zero;

        if (_Camera.GetComponent<CameraScript>().ViewStatus == CameraScript.View.QuaterView)
        {
            if (Input.GetKey(KeyCode.W))
                moveVector += Vector3.forward;
            if (Input.GetKey(KeyCode.S))
                moveVector += Vector3.back;
        }
        if (Input.GetKey(KeyCode.A))
            moveVector += Vector3.left;
        if (Input.GetKey(KeyCode.D))
            moveVector += Vector3.right;
        if (moveVector.sqrMagnitude != 0)
        {
            transform.rotation = Quaternion.Euler(0, 180 + Mathf.Atan2(-moveVector.z, moveVector.x) / Mathf.PI * 180, 0);
            transform.Translate(Vector3.left * Time.deltaTime * Speed);
        }

        if (Input.GetKey(KeyCode.Space) && _InAir == false)
        {
            _InAir = true;
            _Rigidbody.AddForce(Vector3.up * JumpForce);
        }
    }
    
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
            _InAir = false;
    }
}

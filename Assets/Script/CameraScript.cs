﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour 
{    
    public enum View
    {
        SideView,
        QuaterView,
    }
    private View _ViewStatus;
    public View ViewStatus
    {
        get
        {
            return _ViewStatus;
        }
        set
        {
            if (value == _ViewStatus)
            return;
            
            _ViewStatus = value;
            switch (value)
            {
                case View.QuaterView:
                    _DstQuaternion = Quaternion.Euler(60, 315, 0);
                    break;
                    
                case View.SideView:
                    _DstQuaternion = Quaternion.Euler(0, 0, 0);
                    break;
            }
        }
    }
    public bool FollowPlayer;
    public float Distance;
    public bool ChangingView
    {
        get
        {
            Vector3 dist = (transform.rotation.eulerAngles - _DstQuaternion.eulerAngles);
            return dist.sqrMagnitude > 0.1f;
        }
    }

    private GameObject _Player;
    private Quaternion _DstQuaternion;

    private void Start() 
    {
        _Player = GameObject.FindWithTag("Player");

        if (ViewStatus == View.QuaterView)
            transform.rotation = Quaternion.Euler(60, 315, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
        _DstQuaternion = transform.rotation;
    }
    private void FixedUpdate()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, _DstQuaternion, 0.2f);

        if (FollowPlayer == true)
        {
            transform.position = _Player.transform.position - this.transform.rotation * Vector3.forward * Distance;
        }
        if (Input.GetKey(KeyCode.P) && ChangingView == false)
        {
            if (ViewStatus == View.QuaterView)
                ViewStatus = View.SideView;
            else
                ViewStatus = View.QuaterView;
        }
    }
}
